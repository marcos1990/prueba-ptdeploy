import React from 'react';
import { shallow } from 'enzyme';

import Header from '../components/Header';

/**
 * Shallow: es deprecated en react-redux 6 es recomendable usar 'mount'
 */

/* eslint no-undef: 0 */
/* eslint quotes: 0 */

describe('<Pregunta />', () => {
    const wrapper = shallow(<Header />);

    it('Validar definición', () => {
        expect(wrapper).toBeDefined();
    });
    it('Validar número filas', () => {
        expect(wrapper.find('nav').length).toEqual(1);
    });
    it('Validar número filas', () => {
        expect(wrapper.find('Link').text()).toEqual('PTDeploy');
    });
});