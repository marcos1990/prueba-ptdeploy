import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import Pregunta from '../components/Pregunta';
import reducer from '../reducers/preguntaReducer';

/**
 * Shallow: es deprecated en react-redux 6 es recomendable usar 'mount'
 */

/* eslint no-undef: 0 */
/* eslint quotes: 0 */

const mockStore = createStore(reducer);

describe('<Pregunta />', () => {
    const pregunta = {
        id: 649,
        name: 'soyjessie',
        valor: 'OK',
        change_number: 2297080,
    };
    const getWrapper = () => mount(
        <Provider store={mockStore}>
            <Pregunta pregunta={pregunta} />
        </Provider>
    );

    const wrapper = getWrapper();

    it('Validar número filas', () => {
        const rows = wrapper.find('tr');
        expect(rows.length).toEqual(1);
    });
    it('Valida datos en fila', () => {
        const firstRowColumns = wrapper.find('td').map(columns => columns.text());

        expect(firstRowColumns.length).toEqual(3);
        expect(firstRowColumns[0]).toEqual("soyjessie");
        expect(firstRowColumns[1]).toEqual("OK");
    });
    it('Valida botones', () => {
        const lastRowColumns = wrapper.find('td').last().find('button');
        expect(lastRowColumns.length).toEqual(2);
    });
});
