import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import Preguntas from '../components/Preguntas';
import PreguntaReducer from '../reducers/preguntaReducer';


/* eslint no-undef: 0 */
/* eslint quotes: 0 */

describe('<Pregunta />', () => {
    const initialState = {
        preguntas: [],
        listas: [],
        error: null,
        loading: false,
        preguntaselecionada: null
    };
    const mockStore = createStore(PreguntaReducer, initialState);
    const getWrapper = () => mount(
        <Provider store={mockStore}>
            <Preguntas />
        </Provider>
    );

    const wrapper = getWrapper();

    it('Validar definición', () => {
        expect(wrapper).toBeDefined();
    });
});