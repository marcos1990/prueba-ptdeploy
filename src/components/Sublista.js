import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// REDUX
import { useDispatch } from 'react-redux';

import { guardarSublistaAction } from '../actions/sublistaActions';

const Sublista = ({ maquinas }) => {
    const dispatch = useDispatch();

    const [maquinasdata, guardarMaquinas] = useState('');
    // const [maquinasLectura, guardarMaquinasLectura] = useState('');
    const [editar, cambiarEditar] = useState(false);

    useEffect(() => {
        const dataList = maquinas.map(data => (
            data.value
        )).join(' ');
        guardarMaquinas(dataList);
        // guardarMaquinasLectura(data);
        // return () => {}
    }, [maquinas]);

    console.log(maquinas);

    const editarMaquinas = () => {
        cambiarEditar(!editar);
    };

    const guardarMaquinasData = () => {
        cambiarEditar(!editar);

        dispatch(guardarSublistaAction(maquinasdata));
    };

    return (
        <>
            <h1>Datos de Sublistas</h1>
            <table className="table table-striped">
                <thead className="bg-secondary table-dark">
                    <tr>
                        <th scope="col">Datos</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            {
                                editar
                                    ? (
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="maquinas"
                                            value={maquinasdata}
                                            onChange={e => guardarMaquinas(e.target.value)}
                                        />
                                    )
                                    : maquinas.length === 0
                                        ? 'No hay datos'
                                        : maquinasdata
                            }
                        </td>
                        <td>
                            {
                                editar
                                    ? (
                                        <button
                                            className="btn btn-info mr-2"
                                            onClick={guardarMaquinasData}
                                        >
                                            Guardar
                                        </button>
                                    )
                                    : (
                                        <button
                                            className="btn btn-warning mr-2"
                                            onClick={editarMaquinas}
                                        >
                                            Editar
                                        </button>
                                    )
                            }
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    );
};
Sublista.propTypes = {
    maquinas: PropTypes.array.isRequired,
};
export default Sublista;
