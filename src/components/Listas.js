import React from 'react';
import { useHistory } from 'react-router-dom';
// REDUX
import { useSelector } from 'react-redux';
import _ from 'lodash';
// Components
import Lista from './Lista';
import Sublista from './Sublista';


const Listas = () => {
    // const dispatch = useDispatch();
    const history = useHistory();

    const preguntaSeleccionada = useSelector(state => state.preguntas.preguntaselecionada);
    const listado = useSelector(state => state.preguntas.listas);
    const maquinas = useSelector(state => state.sublistas.maquinas);

    // console.log(maquinas);

    if (!preguntaSeleccionada) { history.push('/'); }


    const { name } = preguntaSeleccionada;

    return (
        <>
            <h1>
                Pregunta
                {name}
            </h1>
            <button
                className="btn btn-success mr-2"
            >
                Agregar Lista
            </button>
            <table className="table table-striped">
                <thead className="bg-primary table-dark">
                    <tr>
                        <th scope="col">Nombre Lista</th>
                        <th scope="col">Lista</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        listado.length === 0
                            ? <tr><td>No hay Lista</td></tr>
                            : (_.map(listado, list => (
                                <Lista
                                    key={_.head(list).listgroup}
                                    lista={list}
                                />
                            ))
                            )
                    }
                </tbody>
            </table>
            {
                maquinas.length > 0
                    ? <Sublista maquinas={maquinas} />
                    : null
            }
        </>
    );
};

export default Listas;
