import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary justify-content-between">
        <div className="container">
            <h1 className="color-rojo">
                <Link to="/" className="text-light">
                    PTDeploy
                </Link>
            </h1>
        </div>
    </nav>
);

export default Header;