// external
import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { lecturaSublistasAction } from '../actions/sublistaActions';

const Lista = ({ lista }) => {
    const dispatch = useDispatch();

    const { name } = _.head(lista);

    const listarMaquinas = listaData => {
        const ids = _.map(listaData, 'id');
        dispatch(lecturaSublistasAction(ids));
    };

    return (
        <tr>
            <td>
                {name}
            </td>
            <td>
                {
                    lista.length === 0
                        ? 'No hay sublistas'
                        : lista.length === 1
                            ? `${lista.length} Sublista`
                            : `${lista.length} Sublistas`
                }
            </td>
            <td>
                <button
                    className="btn btn-warning mr-2"
                    onClick={() => listarMaquinas(lista)}
                >
                    Ver listas
                </button>

                <button
                    className="btn btn-success mr-2"
                >
                    Eliminar
                </button>
            </td>
        </tr>
    );
};
Lista.propTypes = {
    lista: PropTypes.array.isRequired,
};
export default Lista;
