import React, { useEffect } from 'react';
// REDUX
import { useSelector, useDispatch } from 'react-redux';
import { obtenerPreguntasAction } from '../actions/preguntaActions';
// Components
import Pregunta from './Pregunta';
import Listas from './Listas';


const Preguntas = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        // consultar API
        const cargarPreguntas = () => dispatch(obtenerPreguntasAction());
        cargarPreguntas();
        // eslint-disable-next-line
    }, []);

    const preguntas = useSelector(state => state.preguntas.preguntas);
    const listado = useSelector(state => state.preguntas.listas);

    return (
        <>
            <h2 className="text-center my-5">
                Listado de Preguntas
            </h2>

            <table className="table table-striped">
                <thead className="bg-primary table-dark">
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Voto</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        preguntas.length === 0
                            ? <tr><td>No hay preguntas</td></tr>
                            : (preguntas.map(pregunta => (
                                <Pregunta
                                    key={pregunta.id}
                                    pregunta={pregunta}
                                />
                            ))
                            )
                    }
                    {
                        listado.length > 0
                            ? <Listas />
                            : null
                    }

                </tbody>
            </table>
        </>
    );
};

export default Preguntas;