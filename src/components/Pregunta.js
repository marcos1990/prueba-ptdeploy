import React from 'react';
import { useHistory } from 'react-router-dom';
// REDUX
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import { obtenerListaAction } from '../actions/preguntaActions';


const Pregunta = ({ pregunta }) => {
    const history = useHistory();
    const dispatch = useDispatch();

    // destructuring
    const { name, valor } = pregunta;

    // función que redirige de una forma programada
    const redireccionarListaPregunta = preguntaData => {
        // pasamos la pregunta al state
        dispatch(obtenerListaAction(preguntaData));

        // redireccionamos una vez hemos pasado la pregunta
        history.push(`/pregunta/${preguntaData.id}`);
    };

    return (
        <>
            <tr>
                <td>{name}</td>
                <td><span className="font-wwight-bold">{valor}</span></td>
                <td className="acciones">
                    <button
                        type="button"
                        className="btn btn-primary mr-2"
                        onClick={() => redireccionarListaPregunta(pregunta)}
                    >
                        Ver Listas
                    </button>
                    <button
                        type="button"
                        className="btn btn-danger"
                    >
                        Eliminar
                    </button>
                </td>
            </tr>
        </>
    );
};
Pregunta.propTypes = {
    pregunta: PropTypes.object.isRequired,
};
export default Pregunta;
