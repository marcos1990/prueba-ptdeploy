import {
    COMENZAR_LECTURA_PREGUNTAS,
    LECTURA_PREGUNTAS_EXITO,
    LECTURA_PREGUNTAS_ERROR,
    COMENZAR_LECTURA_LISTAS,
    LECTURA_LISTA_EXITO,
    LECTURA_LISTA_ERROR
} from '../types';

// cada reducer tiene su propio state
const initialState = {
    preguntas: [],
    listas: [],
    error: null,
    loading: false,
    preguntaselecionada: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case COMENZAR_LECTURA_PREGUNTAS:
        return {
            ...state,
            loading: action.payload
        };
    case LECTURA_PREGUNTAS_EXITO:
        return {
            ...state,
            loading: false,
            error: null,
            listas: [],
            preguntas: action.payload
        };
    case LECTURA_PREGUNTAS_ERROR:
    case LECTURA_LISTA_ERROR:
        return {
            ...state,
            loading: false,
            error: action.payload
        };
    case COMENZAR_LECTURA_LISTAS:
        return {
            ...state,
            loading: true,
            preguntaselecionada: action.payload
        };
    case LECTURA_LISTA_EXITO:
        return {
            ...state,
            loading: false,
            error: null,
            listas: action.payload
        };
    default:
        return state;
    }
}