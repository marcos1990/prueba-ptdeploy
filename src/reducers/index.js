import { combineReducers } from 'redux';
import preguntaReducer from './preguntaReducer';
import sublistaReducer from './sublistaReducer';

export default combineReducers({
    preguntas: preguntaReducer,
    sublistas: sublistaReducer
});