import {
    COMENZAR_LECTURA_SUBLISTAS,
    LECTURA_SUBLISTAS_EXITO,
    LECTURA_PREGUNTAS_ERROR,
} from '../types/index';


const initialState = {
    maquinas: [],
    error: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
    case COMENZAR_LECTURA_SUBLISTAS:
        return {
            ...state,
            loading: action.payload
        };
    case LECTURA_SUBLISTAS_EXITO:
        return {
            ...state,
            loading: false,
            maquinas: action.payload
        };
    case LECTURA_PREGUNTAS_ERROR:
        return {
            ...state,
            loading: false,
            error: action.payload
        };
    default:
        return state;
    }
}