import axios from 'axios';

const clienteAxios = axios.create({
    baseURL: 'http://localhost:4000/'
});

export default clienteAxios;

/**
 * http://localhost:4000/preguntas/649/listas?_embed=listadata
 * http://localhost:4000/listas/650?_embed=listadata
 * http://localhost:4000/preguntas/404?_embed=listas
 */