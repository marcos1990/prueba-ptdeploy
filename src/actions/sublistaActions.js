import {
    COMENZAR_LECTURA_SUBLISTAS,
    LECTURA_SUBLISTAS_EXITO,
    LECTURA_SUBLISTAS_ERROR,
    GUARDAR_SUBLISTA,
} from '../types/index';
import clienteAxios from '../config/axios';


export function lecturaSublistasAction(ids) {
    return async (dispatch) => {
        dispatch(lecturaSublistas());

        try {
            // let maquinas= [];
            // ids.forEach(id => {
            //     consultaSublistas(id).then(

            //     );
            //     // let respuesta = await clienteAxios.get(`/listas/${id}?_embed=listadata`);
            //     maquinas.push(result);
            // })
            const id = ids.pop();
            const maquinas = await clienteAxios.get(`/listas/${id}?_embed=listadata`);
            console.log(maquinas);

            dispatch(lecturaSublistasExito(maquinas.data.listadata));
        } catch (error) {
            console.log(error);
            dispatch(lecturasublistasError());
        }
    };
}

// const consultaSublistas = async (id) => {
//     //let respuesta = await Promise.resolve(clienteAxios.get(`/listas/${id}?_embed=listadata`))
//     let respuesta = await fetch(`http://localhost:4000/listas/${id}?_embed=listadata`);
//     let data = await respuesta.json();
//     return data;
// }

const lecturaSublistas = () => ({
    type: COMENZAR_LECTURA_SUBLISTAS,
    payload: true
});

const lecturaSublistasExito = sublista => ({
    type: LECTURA_SUBLISTAS_EXITO,
    payload: sublista
});

const lecturasublistasError = () => ({
    type: LECTURA_SUBLISTAS_ERROR,
    payload: true
});

//-----------------------------------------------------------------

export function guardarSublistaAction() {
    return (dispatch) => {
        dispatch(guardarSublista());

        // try {

        // } catch (error) {

        // }
    };
}

const guardarSublista = () => ({
    type: GUARDAR_SUBLISTA,
    payload: true
});