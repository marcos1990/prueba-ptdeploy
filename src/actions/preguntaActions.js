import _ from 'lodash';
import {
    COMENZAR_LECTURA_PREGUNTAS,
    LECTURA_PREGUNTAS_EXITO,
    LECTURA_PREGUNTAS_ERROR,
    COMENZAR_LECTURA_LISTAS,
    LECTURA_LISTA_EXITO,
    LECTURA_LISTA_ERROR
} from '../types';
import clienteAxios from '../config/axios';


// Función que lee las preguntas de BBDD
export function obtenerPreguntasAction() {
    return async (dispatch) => {
        dispatch(lecturaPreguntas());

        try {
            const respuesta = await clienteAxios.get('/preguntas');
            dispatch(lecturaPreguntasExito(respuesta.data));
        } catch (error) {
            dispatch(lecturaPreguntasError());
        }
    };
}

const lecturaPreguntas = () => ({
    type: COMENZAR_LECTURA_PREGUNTAS,
    payload: true
});

const lecturaPreguntasExito = preguntas => ({
    type: LECTURA_PREGUNTAS_EXITO,
    payload: preguntas
});

const lecturaPreguntasError = () => ({
    type: LECTURA_PREGUNTAS_ERROR,
    payload: true
});

//--------------------------------------------------------

export function obtenerListaAction(pregunta) {
    return async (dispatch) => {
        dispatch(lecturaListas(pregunta));

        try {
            // const respuesta = await clienteAxios.get
            // (`/preguntas/${pregunta.id}/listas?_embed=listadata`);
            const respuesta = await clienteAxios.get(`/preguntas/${pregunta.id}/listas`);
            const list = _.map(_.groupBy(respuesta.data, 'listgroup'));
            dispatch(lecturaListasExito(list));
        } catch (error) {
            console.log(error);
            dispatch(lecturaListadoError());
        }
    };
}

const lecturaListas = pregunta => ({
    type: COMENZAR_LECTURA_LISTAS,
    payload: pregunta
});

const lecturaListasExito = listas => ({
    type: LECTURA_LISTA_EXITO,
    payload: listas
});

const lecturaListadoError = () => ({
    type: LECTURA_LISTA_ERROR,
    payload: true
});
