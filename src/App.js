import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
// REDUX
import { Provider } from 'react-redux';
// Components
import Header from './components/Header';
import Preguntas from './components/Preguntas';
import Listas from './components/Listas';


import store from './store';

function App() {
    return (
        <Router>
            <Provider store={store}>
                <Header />

                <div className="container mt-5">
                    <Switch>
                        <Route exact path="/" component={Preguntas} />
                        <Route exact path="/pregunta/:id" component={Listas} />
                    </Switch>
                </div>
            </Provider>
        </Router>
    );
}

export default App;